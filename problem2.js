/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. 
            Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");

function createFilesAnddelete(file, filePath) {
  fs.readFile(file, "utf-8", (err, data) => {
    if (err) console.log(err);
    else {
      let upperCase = data.toUpperCase();
      fs.writeFile(`${filePath}/upper.txt`, upperCase, "utf-8", (err) => {
        if (err) console.log(err);
        else {
          fs.writeFile(`${filePath}/filenames.txt`, "upper.txt\n", "utf-8", (err) => {
            if (err) console.log(err);
          }
          );
          fs.readFile(`${filePath}/upper.txt`, "utf-8", (err, data) => {
            if (err) console.log(err);
            else {
              let split = data.toLowerCase().split(".").join("\n");
              fs.writeFile(`${filePath}/split.txt`, split, "utf-8", (err) => {
                if (err) console.log(err);
                else {
                  fs.appendFile(`${filePath}/filenames.txt`, "split.txt\n", "utf-8", (err) => {
                    if (err) console.log(err);
                  }
                  );
                  fs.readFile(`${filePath}/split.txt`, "utf-8", (err, data) => {
                    if (err) console.log(err);
                    else {
                      let sort = data.split("\n").sort().join("\n");
                      fs.writeFile(
                        `${filePath}/sort.txt`,
                        sort,
                        "utf-8",
                        (err) => {
                          if (err) console.log(err);
                          else {
                            fs.appendFile(`${filePath}/filenames.txt`, "sort.txt", "utf-8", (err) => {
                              if (err) console.log(err);
                            }
                            );
                            fs.readFile(`${filePath}/filenames.txt`, "utf-8", (err, data) => {
                              if (err) console.log(err);
                              else {
                                let files = data.split("\n");
                                files.forEach((file, i) => {
                                  setTimeout(() => {
                                    fs.unlink(
                                      `${filePath}/${file}`,
                                      (err) => {
                                        if (err) console.log(err);
                                      }
                                    );
                                  }, (i + 1) * 2000);
                                });
                              }
                            }
                            );
                          }
                        }
                      );
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = { createFilesAnddelete };
