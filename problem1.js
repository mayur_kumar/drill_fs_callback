/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require("fs");
function GenerateRandomJsonAndDelete(path, name) {
  //  create random_json directory
  fs.mkdir(path + name, (err) => {
    if (err) console.log(err);
    else {
      console.log("dir created");
      for (let i = 0; i <= 5; i++) {
        // create random.json file
        fs.writeFile(`${path + name}/random${i}.json`, "[{}]", (err) => {
          if (err) console.log(err);
          else {
            console.log("file created");
            // deleing the created file simultaneously
            setTimeout(() => {
              fs.unlink(`${path + name}/random${i}.json`, (err) => {
                if (err) console.log(err);
                else {
                  console.log("file deleted");
                }
              });
            }, i * 3000);
          }
        });
      }
    }
  });
}

module.exports = { GenerateRandomJsonAndDelete };
