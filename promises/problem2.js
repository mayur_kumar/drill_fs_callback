const fs = require("fs");



function createFilesAnddelete(file, filePath) {
    fs.promises.readFile(file, "utf-8")
        .then((data) => {
            let upperCase = data.toUpperCase();
            return fs.promises.writeFile(`${filePath}/upper.txt`, upperCase, "utf-8");
        })
        .then(() => {
            fs.promises.writeFile(`${filePath}/filenames.txt`, "upper.txt\n", "utf-8");

            return fs.promises.readFile(`${filePath}/upper.txt`, "utf-8");
        })
        .then((data) => {
            let split = data.toLowerCase().split(".").join("\n");
            return fs.promises.writeFile(`${filePath}/split.txt`, split, "utf-8");
        })
        .then(() => {
            fs.promises.appendFile(`${filePath}/filenames.txt`, "split.txt\n", "utf-8");
            return fs.promises.readFile(`${filePath}/split.txt`, "utf-8")
        })
        .then((data) => {
            let sort = data.split("\n").sort().join("\n")
            return fs.promises.writeFile(`${filePath}/sort.txt`, sort, "utf-8",)
        })
        .then(() => {
            fs.promises.appendFile(`${filePath}/filenames.txt`, "sort.txt", "utf-8");
            return fs.promises.readFile(`${filePath}/filenames.txt`, "utf-8")
        })
        .then((data) => {
            let files = data.split('\n')
            files.forEach((file, i) => {
                setTimeout(() => {
                    fs.promises.unlink(`${filePath}/${file}`)
                }, i * 2000)
            })
        })
        .catch((err)=>{
            console.log(err);
        })
}
module.exports = { createFilesAnddelete }
